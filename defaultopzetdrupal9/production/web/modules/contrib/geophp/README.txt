CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

Wraps the geoPHP library: advanced geometry operations in PHP.


REQUIREMENTS
------------

This module does not have any dependency on any other module.


INSTALLATION
------------

Install the geoPHP as you would normally install a contributed Drupal
module. Visit https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
--------------

This module does not have any configuration.


MAINTAINERS
-----------

 * Neslee Canil Pinto: https://www.drupal.org/u/neslee-canil-pinto
