<?php
/**
 * @file
 * Contains \Drupal\htiresponse\HtiresponseController.
 */
namespace Drupal\htiresponse;

use \Drupal\Core\Controller\ControllerBase;

class HtiresponseController extends ControllerBase {
  public function content() {
      $token = \Drupal::request()->request->get('token'); 
    //  $algoritm='RS256';// NB bewaar de keys in pem format anders vreet de php encryptor ze niet
      
  //    $algoritm='SHA256';
      $algoritm=OPENSSL_ALGO_SHA256;
      //https://www.php.net/manual/en/openssl.signature-algos.php
      $fhirversion='R5';
      /*
       * # Generate rsa keys pair
       ssh-keygen -t rsa -b 4096 -f rs256.rsa
       
       # Don't add passphrase
       openssl rsa -in rs256.rsa -pubout -outform PEM -out rs256.rsa.pub
       *
       *
       *
       */
      
      
    //  $secretkey_filepath='/Applications/MAMP/htdocs/HTI/test/portal/defaultopzetdrupal9/jwtRS256.key';
    // localhost
      $publickey_filepath='/Applications/MAMP/htdocs/HTI/test/portal/defaultopzetdrupal9/jwtRS256.key.pub';
      // teset server
      // file_get_contents(DRUPAL_ROOT . '/core/tests/fixtures/test_missing_base_theme/test_missing_base_theme.info.yml'),
      $publickey_filepath= DRUPAL_ROOT . '/jwtRS256.key.pub';
      //  $secretkey_filepath='/Applications/MAMP/htdocs/HTI/test/portal/defaultopzetdrupal9/jwtRS256.key';
      //  $publickey_filepath='/Applications/MAMP/htdocs/HTI/test/portal/defaultopzetdrupal9/jwtRS256.key.pub';
      /*
       *
       * # Generate rsa keys pair
       ssh-keygen -t rsa -b 4096 -f rs256.rsa
       
       *
       *
       */
      
   //   $secretkey_filepath='/Applications/MAMP/htdocs/HTI/test/portal/defaultopzetdrupal9/xrs256.rsa';
   //   $publickey_filepath='/Applications/MAMP/htdocs/HTI/test/portal/defaultopzetdrupal9/xrs256.rsa.pub';
      //knip token weer in 3-en

      $tokenarray=explode('.',$token);
      $signature=HtiresponseController::base64_decode_url($tokenarray[2]);
      
//      $fp = fopen('/Applications/MAMP/htdocs/HTI/test/portal/defaultopzetdrupal9/production/web/data.txt', 'a');//opens file in append mode.
//      fwrite($fp,$signature);
//      fwrite($fp, "\n\n");
//      fclose($fp);
      
      $verify=HtiresponseController::verifyJWTPublic($tokenarray[0].'.'.$tokenarray[1],$signature,$publickey_filepath, $algoritm);
      if ($verify==1){
          $decryptedsignature="OK";
      } else {
          $decryptedsignature="NOT OK";
      }
      // nu checken of iat + 5*60 < exp  (zo niet, dan error)
      // checken of jti niet al eerder is uitgevored, in dat geval error
      
    return array(
      '#type' => 'markup',
        '#markup' => t('header:'.HtiresponseController::base64_decode_url($tokenarray[0]).'  load:'.HtiresponseController::base64_decode_url($tokenarray[1]).'  secret:'.$tokenarray[2].'     --->  '.$decryptedsignature),
    );
  }
  
  private function verifyJWTPublic($data, $signature,$path, $algoritm)
  {
      $fcontents = file_get_contents($path);
      
      
      
      
  //    $str = chunk_split($fcontents, 64, "\n");
  //    $key = "-----BEGIN PUBLIC KEY-----\n$str-----END PUBLIC KEY-----\n";
  //    $publicKey=openssl_get_publickey($fcontents);
  //    return openssl_verify( $data, $signature, $publicKey,$algoritm );
      
      
      
      $publicKey=openssl_get_publickey($fcontents);
      return openssl_verify( $data, $signature, $publicKey,$algoritm );
  }
  private function base64_encode_url($string) {
      return str_replace(['+','/','='], ['-','_',''], base64_encode($string));
  }
  
  private function base64_decode_url($string) {
      return base64_decode(str_replace(['-','_'], ['+','/'], $string));
  }
}